## Prefab Scripting - 3D

**THANK YOU SO MUCH FOR SUPPORTING THE WORK. YOU ARE AWESOME :)**

**Note**: This Asset is a TOOL and not a resource pack.

# Short Description

Prefab Scripting 3D version is a combination of the following tools:-

**3D character** : 3D fps Character

**3D Shooter** : 3D Character with the ability to shoot.

**Destroyer** : Destroys an object with respect to delay, on start and many other conditions.

**MouseDrag** : An object which can be dragged with mouse.

**Object Follower** : An object which follows another object(if the object has transform and is draged and dropped into the script).

**3D platformer** : A player(platformer) with ability to move only in two directions(front/back)

**Look At** : Rotates and looks at the object specified.

**Set Rotation** : Set the object to rotate in a pirticular direction/directions

**Set Movement** : Moves from a point to a direction provided.Also has the ability to destroy itself on delay after some time.(can be disabled)

**3D Spawner** : Spawns an object along x, y and z axis after a delay with a uniform or non-unirform time gaps in between.

# Long Description

Prefab Scripting is a simple to use set of tools to be used. It's made with customization, and effeciency kept in mind for the user to have good experience.

Here are the documentation on the all the tools:-

1. 3D character :
    3D character is simply an fps character made to move on input
    The Following options can be found in the Player 3D script.
   
   1. Camera : Drag your Camera here to Set your camera for fps.
   2. Player : Drag the game object which should be the fps.
   3. Mouse Sensitivity : The Sensitivity of the mouse while looking around.
   4. Speed : the speed through which the player would move.
   5. gravity : the force through which the fps falls down.
   6. Jump height : The height till the fps jumps.
      Other than these there are some customization options which is highly recommended. Customizing the following is highly recmommended.
   7. the fps object.
   8. The Looks.
   9. Animation, etc

2. 3D player Shooter:-
    3D Shooter is simply a 3D character but with an extra ability to shoot:-
    The Following options can be found in the "Player Shooter 3d" script:-
    (NOTE: the default bullet uses the "set movement 3d" tool to move)
   
   1. Player : The object from which the bullet is shot.
   2. FirePoint: The transform from which the bullet is shot.
   3. Bullet : The prefab/gameobject which will be shot.
      Recommended:-
   4. The gun prefab
   5. bullet
   6. player Looks

3. Destroyer:-
    As the name suggests, Destroyer destroys an object with some conditions.
    The Following options can be found in the Player Shooter script:-
   
   1. Object to destroy: drag and drop the object which you wish to be destroyed in the game.
   2. Destroy Time: destroy a game object on a delay of time given.(works inly if "Destroy on delay" bool is checked)
   3. Destroy on start:(Not Recommended) destroys an object on game start. Can be done with some delay.(delay works only if "Destroy on delay" bool is checked)

4. Mouse Drag:-
   This is simply an object which can be dragged using a mouse.
   To use this, simply attach the "MouseDrag" script to any object
   or, use the "3D Dragable Object button" (3D prefabs/ Prefab Script tab) to create one.

5. Object Follower:-
    Object Follower is a tool which should be used when you want an object to follow another object.
   
   1. Target: The object which will be followed by the Object Follower.
   2. Follower : The object which will follow.
   3. Move Speed: Speed through which the object is being followed by the object follower

6. 3D Platformer:-
    This will give you a 3d platformer to move front/back and set your camera with smooth movements. It consists of "look at" script along to help you with camera.
   
   1. Player Platformer : The platformer which will move on input.
   2. Main Camera : The main camera which looks at the platformer.
   3. Move Speed : The speed with which the platformer moves.
   4. Camera transition speed : The smoothnes of movement of camera.
      For all the three, drag the an object which has the transform you dezire.

7. LookAt:-
    Look At has the ability to to rotate towards and look towards any object
   
   1. Target : The transform which will be looked at.
   2. Look At : This is the object which looks at the target.
      Customize:-
   3. Target target
   4. Look At object

8. Set Movement:-
    This moves in any set direction (x, y, z/or any combination of the axises)
   
   1. Object to move : This is the object which moves.
   2. x/y/z : specify the direction it moves
   3. Move Speed : The speed through which the object moves.
   4. Destroy on Delay : destroys after some time after creating. (used in bullets)
   5. Delay : The time of delay to destroy itself.(works only if Destroy on delay is checked).

9. Set Rotation:-
    Sets the object to rotate along an axis
   
   1. Object to rotate : the object which would rotate.
   
   2. x/y/z : The axis along which the object rotates.
   
   3. rotate speedd : The speed through which the object rotates.
   
   4. Destroy on Delay : Check if you want it to destroy itself after some time.
   
   5. Delay : The time(in seconds) after which the object destroys itself. (Works only if Destroy on Delay is checked)
   
   6. Looks
   
   7. Delay time as you desire

10. 3D Spawner
    Spawns a pirticular object in a range  you specify
    
    1. Object To Spawn: The game object which would be spawned(which you should specify)
    2. Start Spawn Delay: Time After which the spawn starts 
    3. RandomizeRespawnTimeGaps: Should be false if you want uniform time between two spawns. This should be checked true if you want a non-uniform random time with set range, between two spawns.
    4. Respawn Time Gap: The time gap between 2 spawns of the object. This works only if RandomizeRespawnTimeGaps is false. This lets you have uniform time between two spawns.
    5. RespawnTimeRange1 & RespawnTimeRange2:  This works only if RandomizeRespawnTimeGaps is True. A random float number between RespawnTimeRange1 & RespawnTimeRange2 will be chosen as a time gap between two spawns each time.
    6. The random value of x coordinate will be chosen throught the range given from x1 to x2
    7. The random value of y coordinate will be chosen throught the range given from y1 to y2
    8. The random value of z coordinate will be chosen throught the range given from z1 to z2

## Conclusion

Well... If you have used the asset you would propably realize that the tools does not "look" good. It is really not made for that. It is made for simply made to save your time. Example: It saves time by giving you the whole framework of a menu, but it also encorages you to try out customizing it yourself as well.

If you have furthur problems, 
head hover to FadinGeek Channel where there will be videos posted.
You'll find the link to that in the help tab in unity.

That's about it... 
I hope you enjoy using this asset as much as we enjoyed making it :)
Thank You 
## . . . FadinGeek . . .
